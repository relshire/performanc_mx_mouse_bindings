#!/bin/bash
# This is a shell script to start xbindkeys. It is necessesary to have
# xbindkeys running for this setup to work. Auto start this at login for 
# your window manager.
xbindkeys -n
