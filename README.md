# performance_mx_mouse_bindings

This repo is for code and information for making use of the zoom and thumb buttons on the Logitech Performance MX mouse in Debian 10. As configured here, the zoom button with the scroll wheel controls text size in the active window. The thumb button is mapped to control-F10 which reveals all windows in KDE.

**Credit:** I used instructions and the config script [here](https://www.ralf-oechsner.de/opensource/page/logitech_performance_mx) and modified that to suit.

**To get started, install the dependencies:**
`sudo apt-get update && sudo apt-get install xbindkeys xautomation -y`

**Install the xbindkey config and the run script:**

`~/.xbindkeysrc.scm`

`~/.kde/Autostart/xbindkeys.sh`

**Configure your window manager to autostart the script:**

KDE Plasma 5 [instructions](https://www.addictivetips.com/ubuntu-linux-tips/autostart-programs-on-kde-plasma-5/).

GNOME [instructions](https://www.addictivetips.com/ubuntu-linux-tips/autostart-programs-on-gnome-shell/).
