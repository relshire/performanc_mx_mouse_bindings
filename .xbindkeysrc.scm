;;   This configuration is guile based.
;;   http://www.gnu.org/software/guile/guile.html
;;   This config script is supposed to live in the homedirectory and be named .xbindkeysrc.scm
;;   Based on the script by Zero Angel posted at
;;   http://www.linuxquestions.org/questions/linux-desktop-74/%5Bxbindkeys%5D-advanced-mouse-binds-4175428297/
;;   And adapted by Camilo Rada
;;
;;   It is intended to work with the Logitech Performance MX Mouse
;;   It allows to zoom with the wheel while the zoom button is pressed
;;   It also allows to assing an action to a single press-and-release of the zoom button
;;   The action assigned to that action is Super+z but is can be modified
;;   Additional Modification by Rob Elshire
;;   Added use of the thumb button. Assigned to Ctrl+F10 


(define actionperformed 0)

;; The first-binding loop can contain bindings / actions for multiple buttons / keys.
;; Add new ones as you like.

(define (first-binding)
"First binding"
;; Logitech Zoom Button
(xbindkey-function '("b:13") b13-second-binding)

;; Logitech Thumb Button
(xbindkey-function '("b:10")
            (lambda ()
;; Emulate Ctrl+F10 (Show All Windows)
		(run-command "/usr/bin/xte 'keydown Control_L' 'key F10' 'keyup Control_L' &")))

;; You can add additional bindings before the next paren.		
)


(define (reset-first-binding)
"reset first binding"
(ungrab-all-keys)
(remove-all-keys)
;; Set Action Performed state back to 0
(set! actionperformed 0)
;; Forcefully release all modifier keys!
(run-command "/usr/bin/xte 'keyup Control_L' 'keyup Alt_L' 'keyup Shift_L' 'keyup Super_L' &")

(first-binding)
(grab-all-keys))

;; The b13-second-binding section adds actions for when the b13 (zoom button) is pressed
;; and held down. An action is also defined for when the zoom button is pressed and released
;; without being combined with other button actions. In this case control-z.
(define (b13-second-binding)
"Zoom Button Extra Functions"
(ungrab-all-keys)
(remove-all-keys)

;; Scroll Up
(xbindkey-function '("b:4")
                (lambda ()
;; Emulate Ctrl+plus (Zoom in)
                (run-command "/usr/bin/xte 'keydown Control_L' 'key plus' 'keyup Control_L' &")
        (set! actionperformed 1)
))

;; Scroll Down
(xbindkey-function '("b:5")
                (lambda ()
;; Emulate Ctrl+Alt+minus (Workspace Down)
                (run-command "/usr/bin/xte 'keydown Control_L' 'key minus' 'keyup Control_L' &")
        (set! actionperformed 1)
))

(xbindkey-function '(release "b:13") (lambda ()
;; Perform Action if Button 8 is pressed and released by itself
(if (= actionperformed 0) (run-command "/usr/bin/xte 'keydown Super_L' 'key z' 'keyup Super_L' &"))
(reset-first-binding)))
(grab-all-keys))

;; (debug)
(first-binding)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; End of xbindkeys configuration ;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
